<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group(['prefix' => 'auth'], function() {
    Route::post('sign-in', [AuthController::class, 'login']);
});

Route::group(['prefix' => 'auth', 'middleware' => ['auth:sanctum']], function() {
    Route::get('me', [AuthController::class, 'me']);
});

Route::group(['prefix' => 'admin', 'middleware' => ['auth:sanctum']], function() {
    Route::group(['prefix' => 'products'], function() {
        Route::get('/', [ProductController::class, 'index']);
        Route::post('/', [ProductController::class, 'create']);
        Route::get('/{product}', [ProductController::class, 'product']);
        Route::put('/{product}', [ProductController::class, 'update']);
        Route::delete('/{product}', [ProductController::class, 'delete']);
    });

    Route::group(['prefix' => 'orders'], function() {
        Route::get('/', [OrderController::class, 'index']);
        Route::put('/{order}', [OrderController::class, 'update']);
    });
});

Route::group(['prefix' => 'products'], function() {
    Route::get('/', [\App\Http\Controllers\ProductController::class, 'index']);
    Route::get('/{product}', [\App\Http\Controllers\ProductController::class, 'product']);
});

Route::get('delivery-services', [\App\Http\Controllers\OrderController::class, 'deliveryServices']);

Route::group(['prefix' => 'orders'], function() {
    Route::post('/', [\App\Http\Controllers\OrderController::class, 'create']);
});
