<?php

namespace Database\Seeders;

use App\Models\Product;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductQuantitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
//        Custom factory based on database records

        $faker = Factory::create();

        $product_ids = array_values(Product::pluck('id')->all());

        for ($i = 0; $i <= count($product_ids) / 2; $i++) {
            $data = [
                'product_id' => $faker->randomElement($product_ids),
                'quantity' => $faker->numberBetween(1, 50),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];

            DB::table('product_quantities')->insert($data);
        }
    }
}
