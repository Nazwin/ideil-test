<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class DeliveryServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            [
                'name' => 'Нова Пошта',
                'description' => 'Для доставки потрібно вказати населений пункт та номер відділення Нової Пошти.',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Укрпошта',
                'description' => 'Для доставки потрібно вказати населений пункт, поштовий індекс та вашу адресу.',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        ];

        foreach ($data as $deliveryService) {
            DB::table('delivery_services')->updateOrInsert(
                ['name' => $deliveryService['name']],
                $deliveryService
            );
        }
    }
}
