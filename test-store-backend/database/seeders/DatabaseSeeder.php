<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Client;
use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        DB::table('users')->updateOrInsert(
            ['email' => 'admin@app.com', 'name' => 'First Admin'],
            ['password' => Hash::make('password'), 'created_at' => now(), 'updated_at' => now()]
        );

        Product::factory(100)->create();

        $this->call(ProductQuantitySeeder::class);

        Client::factory(50)->create();

        $this->call(DeliveryServiceSeeder::class);

        $this->call(OrderSeeder::class);
    }
}
