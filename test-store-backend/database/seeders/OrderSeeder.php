<?php

namespace Database\Seeders;

use App\Models\Client;
use App\Models\DeliveryService;
use App\Models\Order;
use App\Models\Product;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
//        Custom factory based on database records

        $faker = Factory::create();

        $product_ids = array_values(Product::take(100)->pluck('id')->all());

        $clients = Client::select('id', 'name')->limit(100)->get();

        $delivery_service_ids = array_values(DeliveryService::pluck('id')->all());

        for ($i = 0; $i <= 50; $i++) {
            $client = $faker->randomElement($clients);

            $data = [
                'client_id' => $client->id,
                'full_name' => $client->name,
                'delivery_service_id' => $faker->randomElement($delivery_service_ids),
                'delivery_address' => $faker->address,
                'status' => $faker->randomElement(['pending', 'sent', 'delivered', 'received', 'returned']),
                'comment' => $faker->randomElement(['', $faker->text()]),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];

            DB::table('orders')->insert($data);
        }

//        $orders_ids = array_values(Order::pluck('id')->all());
//
//        foreach ($orders_ids as $order_id) {
//            $data = [
//                'order_id' => $order_id,
//                'product_id' => $faker->randomElement($product_ids),
//                'quantity' => 1,
//                'created_at' => Carbon::now(),
//                'updated_at' => Carbon::now()
//            ];
//        }
    }
}
