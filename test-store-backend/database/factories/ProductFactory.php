<?php

namespace Database\Factories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->name,
            'uid' => Str::uuid(),
            'images' => json_encode([$this->faker->imageUrl(), $this->faker->imageUrl()]),
            'price' => $this->faker->randomFloat(2, 10, 1000),
            'status' => $this->faker->boolean(90),
        ];
    }
}
