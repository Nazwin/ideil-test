<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Product extends Model
{
    use HasFactory;

    protected $fillable =[
        'name',
        'uid',
        'images',
        'price',
        'status'
    ];

    public function productQuantity() : HasOne
    {
        return $this->hasOne(ProductQuantity::class);
    }

    public function orders() : HasManyThrough
    {
        return $this->hasManyThrough(
            Order::class,
            OrderProduct::class,
            'product_id',
            'id',
            'id',
            'id'
        );
    }

    public function ordersPivot() : HasMany
    {
        return $this->hasMany(OrderProduct::class);
    }

    public function getAvailableAttribute() : int
    {
        return $this->productQuantity
            ? $this->productQuantity->quantity -
                $this->ordersPivot()
                    ->where('created_at', '>', $this->productQuantity->updated_at)
                    ->sum('quantity')
            : 0;
    }
}
