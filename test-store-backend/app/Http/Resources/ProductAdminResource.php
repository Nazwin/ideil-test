<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductAdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'uid' => $this->uid,
            'images' => array_map(function ($image) {
                if(!str_contains($image, 'http')) {
                    return config('app.url') . $image;
                }
                return $image;
            }, json_decode($this->images)),
            'price' => $this->price,
            'status' => $this->status,
            'quantity' => $this->available,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
