<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'client_id' => $this->client_id,
            'full_name' => $this->full_name,
            'delivery_service_name' => $this->delivery_service->name,
            'delivery_address' => $this->delivery_addres,
            'status' => $this->status,
            'comment' => $this->comment,
            'items' => OrderProductResource::collection($this->products)
        ];
    }
}
