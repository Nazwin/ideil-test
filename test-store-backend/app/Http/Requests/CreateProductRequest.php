<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'uid' => 'nullable|string',
            'price' => 'required|numeric',
            'status' => 'required|boolean',
            'quantity' => 'required|integer',
            'images.*' => 'required|image|mimes:jpeg,jpg,png|max:5120',
        ];
    }
}
