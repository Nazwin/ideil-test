<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request) : JsonResponse
    {
        $data = $request->validate([
            'email' => 'required|email',
            'password' => 'required|string',
        ]);

        $user = User::where(['email' => $data['email']])->first();

        if(!$user) {
            return response()->json(['type' => 'email', 'message' => 'No user found.'], 404);
        }

        if(!Auth::attempt($data)) {
            return response()->json(['type' => 'password', 'message' => 'Wrong password.'], 401);
        }

        $token = $user->createToken('api')->plainTextToken;

        return response()->json([
            'user' => new UserResource($user),
            'access_token' => $token,
        ]);
    }

    public function logout() : Response
    {
        Auth::user()->currentAccessToken()->delete();

        return response()->noContent();
    }

    public function me() : JsonResponse
    {
        return response()->json([
            'user' => new UserResource(Auth::user()),
        ]);
    }
}
