<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::where(['status' => 1])->paginate(20);

        $result = $products->toArray();
        $result['data'] = ProductResource::collection($products);

        return response()->json($result);
    }
}
