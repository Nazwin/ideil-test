<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use App\Models\Client;
use App\Models\DeliveryService;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function create(Request $request) : JsonResponse|OrderResource
    {
        $data = $request->validate([
            'cartItems' => 'required|array',
            'cartItems.*.id' => 'required|numeric',
            'cartItems.*.count' => 'required|numeric',
            'client' => 'required',
            'client.fullName' => 'required|string',
            'client.phoneNumber' => 'required|string',
            'client.address' => 'required|string',
            'client.deliveryService' => 'required',
        ]);

        $attachProducts = [];

        foreach ($data['cartItems'] as $cartItem) {
            $product = Product::find($cartItem['id']);
            if(!$product) {
                return response()->json(['message' => 'Product not found.'], 404);
            } elseif ($cartItem['count'] > $product->available) {
                return response()->json(['message' => "There is not enough {$product->name} in stock."], 409);
            }
            $attachProducts[$cartItem['id']] = ['quantity' => $cartItem['count']];
        }

        $client = Client::create([
            'name' => $data['client']['fullName'],
            'phone' => $data['client']['phoneNumber'],
        ]);

        $order = Order::create([
            'client_id' => $client->id,
            'full_name' => $client->name,
            'delivery_service_id' => $data['client']['deliveryService']['id'],
            'delivery_address' => $data['client']['address']
        ]);

        $order->products()->attach($attachProducts);

        return new OrderResource($order);
    }

    public function deliveryServices() :JsonResponse
    {
        $deliveryServices = DeliveryService::all();

        return response()->json($deliveryServices);
    }
}
