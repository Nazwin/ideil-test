<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateProductRequest;
use App\Http\Resources\ProductAdminResource;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Models\ProductQuantity;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function index() : JsonResponse
    {
        $products = Product::orderBy('created_at', 'desc')->paginate(20);

        $result = $products->toArray();
        $result['data'] = ProductAdminResource::collection($products);

        return response()->json($result);
    }

    public function create(CreateProductRequest $request) : ProductAdminResource
    {
        $imagePaths = [];
        if ($request->hasFile('images')) {
            foreach ($request->file('images') as $image) {
                $path = $image->store('product_images', 'public');
                $imagePaths[] = Storage::url($path);
            }
        }

        $product = Product::create([
            'name' => $request->input('name'),
            'uid' => $request->input('uid'),
            'price' => $request->input('price'),
            'status' => $request->input('status'),
            'images' => json_encode($imagePaths),
        ]);

        $quantity = $request->input('quantity', 0);
        ProductQuantity::create([
            'product_id' => $product->id,
            'quantity' => $quantity,
        ]);

        return new ProductAdminResource($product);
    }

    public function product(Request $request, Product $product) : ProductAdminResource
    {
        return new ProductAdminResource($product);
    }

    public function update(CreateProductRequest $request, Product $product) : ProductAdminResource
    {
        $product->fill($request->validated());
        $product->save();

        if($product->productQuantity->quantity !== $request->get('quantity')) {
            $product->productQuantity()->update(['quantity' => $request->get('quantity')]);
        }

        return new ProductAdminResource($product);
    }
}
