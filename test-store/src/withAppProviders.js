import React, { useState, useEffect } from 'react';
import Provider from 'react-redux/es/components/Provider';
import routes from "./configs/routesConfig";
import store from './store';
import AppContext from './AppContext';

const withAppProviders = (Component) => (props) => {


    const WrapperComponent = () => (
        <AppContext.Provider
            value={{
                routes,
                // cart,
                // addToCart,
                // removeItem,
                // increase,
                // decrease
            }}
        >
            <Provider store={store}>
                <Component {...props} />
            </Provider>
        </AppContext.Provider>
    );

    return WrapperComponent;
};

export default withAppProviders;
