const sessionStorageKey = "fuseForceRedirectUrl";

export const getSessionForceRedirectUrl = () => {
  return window.sessionStorage.getItem(sessionStorageKey);
};

export const setSessionForceRedirectUrl = (url) => {
  window.sessionStorage.setItem(sessionStorageKey, url);
};

export const resetSessionForceRedirectUrl = () => {
  window.sessionStorage.removeItem(sessionStorageKey);
};
