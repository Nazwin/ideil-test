import logo from './logo.svg';
import './App.css';
import withAppProviders from "./withAppProviders";
import {RouterProvider} from "react-router-dom";
import routes from "./configs/routesConfig";
import { Provider } from 'react-redux';
import store from "./store";
import {CartContext} from "./CartContext";
import React, { useState, useEffect } from 'react';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';

const darkTheme = createTheme({
    palette: {
        mode: 'dark',
    },
});

const defaultValue = localStorage.getItem('cart')
    ? {cartItems: JSON.parse(localStorage.getItem('cart'))}
    : {cartItems: []};

function App() {
    const [cart, setCart] = useState(defaultValue);

    useEffect(() => {
        const storedCart = localStorage.getItem('cart');

        if (storedCart) {
            setCart({cartItems: JSON.parse(storedCart)});
        }
    }, []);

    useEffect(() => {
        localStorage.setItem('cart', JSON.stringify(cart.cartItems));
    }, [cart]);

    const addToCart = (product) => {
        if (product.available <= 0) return;

        setCart({
            cartItems: cart.cartItems.find(item => item.id === product.id) ?
                cart.cartItems.map(item =>
                    item.id === product.id ?
                        {...item, count: item.count + 1} :
                        item
                ) :
                [...cart.cartItems, {...product, count: 1}]
        });
    };

    const removeItem = (id) => {
        setCart({
            cartItems: cart.cartItems.filter(item => item.id !== id)
        })
    };

    const increase = (product) => {
        setCart({
            cartItems: cart.cartItems.map(item =>
                item.id === product.id
                    ? {...item,
                        count: product.available >= item.count + 1
                            ? item.count + 1
                            : product.available
                    }
                    : item
            )
        });
    };

    const decrease = (product) => {
        setCart({
            cartItems: cart.cartItems.map(item =>
                item.id === product.id ?
                    {...item, count: item.count > 1 ? item.count -1 : 1} :
                    item
            )
        })
    }

  return (
    <div className="App">
        <ThemeProvider theme={darkTheme}>
            <CssBaseline />
            <CartContext.Provider
                value={{
                    cart,
                    addToCart,
                    removeItem,
                    increase,
                    decrease
                }}
            >
                <Provider store={store}>
                    <RouterProvider router={routes} />
                </Provider>
            </CartContext.Provider>
        </ThemeProvider>
    </div>
  );
}

export default withAppProviders(App)();
