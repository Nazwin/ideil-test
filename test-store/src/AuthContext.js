import { useEffect, useState, useContext, createContext } from "react";
import { useDispatch } from "react-redux";
import {apiService} from "./services";
import { signInWithToken } from "./services/authOperations";
import {LinearProgress} from "@mui/material";
import {useNavigate} from "react-router-dom";

const AuthContext = createContext();

function AuthProvider({ children }) {
    const [waitAuthCheck, setWaitAuthCheck] = useState(true);
    const [isAuthenticated, setIsAuthenticated] = useState(undefined);
    const [userRole, setUserRole] = useState(undefined);

    const dispatch = useDispatch();

    const navigate = useNavigate();

    useEffect(() => {

        apiService.on("onAutoLogin", refreshUserAuthData);

        apiService.on("onLogin", (user) => {
            success(user);
        });

        apiService.on("onNoAccessToken", () => {
            pass();
        });

        apiService.init();

        function refreshUserAuthData(afterVerify = false) {
            /**
             * Sign in and retrieve user data with stored token
             */
            signInWithToken()
                .then((user) => {
                    success(user, null, afterVerify);
                })
                .catch((error) => {
                    pass(error.message);
                });
        }

        function success(user, message, afterVerify = false) {
            Promise.all([]).then((values) => {
                setUserRole('admin');
                setWaitAuthCheck(false);
                setIsAuthenticated(true);
            });
        }

        function pass(message) {
            setWaitAuthCheck(false);
            setIsAuthenticated(false);

            apiService.setAccessToken(null);

            navigate('sign-in');
        }
    }, [dispatch]);

    return waitAuthCheck ? (
        <LinearProgress />
    ) : (
        <AuthContext.Provider
            value={{
                isAuthenticated,
                userRole,
            }}
        >
            {children}
        </AuthContext.Provider>
    );
}

function useAuth() {
    const context = useContext(AuthContext);
    if (context === undefined) {
        throw new Error("useAuth must be used within a AuthProvider");
    }
    return context;
}

export { AuthProvider, useAuth };
