import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";
import {apiBaseQuery} from "../../../services";

const productsAdminApi = createApi({
    reducerPath: "productsAdminApi",
    baseQuery: apiBaseQuery({
        baseRoute: "admin",
    }),
    tagTypes: ["Products"],
    endpoints: (build) => ({
        getProducts: build.query({
            query: ({page}) => ({url: `products?page=${page ?? 1}`}),
            transformResponse(baseQueryReturnValue, meta, arg) {
                return baseQueryReturnValue
            },
            providesTags: (result = []) => [
                ...result.data.map(({id}) => ({type: "Products", id})),
                {type: "Products", id: "LIST"},
            ]
        }),
        getProduct: build.query({
            query: ({id}) => ({url: `products/${id}`}),
            transformResponse(baseQueryReturnValue, meta, arg) {
                return baseQueryReturnValue.data
            }
        }),
        createProduct: build.mutation({
            query: (data) => ({
                url: "products",
                method: "POST",
                data,
            }),
            invalidatesTags: () => {
                return [{ type: "Products", id: "LIST" }];
            },
            transformResponse: (response) => response.data,
        }),
        updateProduct: build.mutation({
            query: ({ id, ...data }) => ({
                url: `products/${id}`,
                method: "PUT",
                data,
            }),
            onQueryStarted({ id, ...patch }, { dispatch, queryFulfilled }) {
                try {
                    const patchResult = dispatch(
                        productsApi.util.updateQueryData("getProducts", undefined, (draft) => {
                            return [...draft].map((product) => {
                                if (product.id === id) {
                                    return { ...product, ...patch };
                                }

                                return product;
                            });
                        })
                    );

                    queryFulfilled.catch((error) => {
                        patchResult.undo();
                    });
                } catch (error) {
                    console.log("error", error);
                }
            },
        }),
        deleteProduct: build.mutation({
            query: (id) => ({
                url: `products/${id}`,
                method: "DELETE",
            }),
            invalidatesTags: () => [{ type: "Products", id: "LIST" }],
        }),
    })
})

export const {
    useGetProductQuery,
    useGetProductsQuery,
    useCreateProductMutation,
    useUpdateProductMutation,
    useDeleteProductMutation,
} = productsAdminApi;

export default productsAdminApi;