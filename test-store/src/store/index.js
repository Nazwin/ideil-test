import { configureStore, combineReducers } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";
import productsApi from "./public/products/productsApi";
import productsAdminApi from "./admin/products/productsApi";
import checkoutApi from "./public/checkout/checkoutApi";

const store = configureStore({
    reducer: combineReducers({
        [productsApi.reducerPath]: productsApi.reducer,
        [checkoutApi.reducerPath]: checkoutApi.reducer,
        [productsAdminApi.reducerPath]: productsAdminApi.reducer,
    }),
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
            immutableCheck: false,
            serializableCheck: false,
        }).concat([productsApi.middleware, checkoutApi.middleware, productsAdminApi.middleware]),
    devTools: process.env.NODE_ENV === "development",
});

setupListeners(store.dispatch);
export default store;