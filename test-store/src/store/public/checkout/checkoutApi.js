import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";
import {apiBaseQuery} from "../../../services";

const checkoutApi = createApi({
    reducerPath: "checkoutApi",
    baseQuery: apiBaseQuery({
        baseRoute: "/",
    }),
    tagTypes: ["DeliveryServices", "Orders"],
    endpoints: (build) => ({
        getDeliveryServices: build.query({
            query: (arg) => ({url: "delivery-services"}),
            transformResponse(baseQueryReturnValue, meta, arg) {
                return baseQueryReturnValue
            },
            providesTags: (result = []) => [
                ...result.map(({id}) => ({type: "DeliveryServices", id})),
                {type: "DeliveryServices", id: "LIST"},
            ]
        }),
        createOrder: build.mutation({
            query: (data) => ({
                url: "orders",
                method: "POST",
                data,
            }),
            invalidatesTags: () => {
                return [{ type: "Orders", id: "LIST" }];
            },
            transformResponse: (response) => response.data,
        }),
    })
})

export const {
    useGetDeliveryServicesQuery,
    useCreateOrderMutation,
} = checkoutApi;

export default checkoutApi;