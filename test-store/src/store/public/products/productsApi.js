import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";
import {apiBaseQuery} from "../../../services";

const productsApi = createApi({
    reducerPath: "productsApi",
    baseQuery: apiBaseQuery({
        baseRoute: "/",
    }),
    tagTypes: ["Products"],
    endpoints: (build) => ({
        getProducts: build.query({
            query: ({page}) => ({url: `products?page=${page ?? 1}`}),
            transformResponse(baseQueryReturnValue, meta, arg) {
                return baseQueryReturnValue
            },
            providesTags: (result = []) => [
                ...result.data.map(({id}) => ({type: "Products", id})),
                {type: "Products", id: "LIST"},
            ]
        })
    })
})

export const {
    useGetProductsQuery,
} = productsApi;

export default productsApi;