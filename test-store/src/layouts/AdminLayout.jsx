import {AuthProvider} from "../AuthContext";
import {AppBar, Badge, Button} from "@mui/material";
import Box from "@mui/material/Box";
import {useNavigate} from "react-router-dom";

function AdminLayout({children}) {

    const navigate = useNavigate();

    const handleLogout = () => {

    };

    return (
        <AuthProvider>
            <AppBar position="sticky">
                <Box style={{padding: "15px 0", display: "flex", justifyContent: "center", gap: "30px"}}>
                    <Button variant="text" onClick={() => navigate("/admin/products")}>Manage products</Button>
                    <Button variant="text" onClick={() => navigate("/admin/orders")}>Orders</Button>
                    <Button variant="text" onClick={() => handleLogout}>Sign out</Button>
                </Box>
            </AppBar>
            <Box>
                {children}
            </Box>
        </AuthProvider>
    );
}

export default AdminLayout;