import {AppBar, Badge, Button, Link} from "@mui/material";
import {ShoppingCartOutlined} from "@mui/icons-material";
import {useContext} from "react";
import {CartContext} from "../CartContext";
import Box from "@mui/material/Box";
import {useNavigate} from "react-router-dom";

function PublicLayout({children}) {
    const { cart } = useContext(CartContext);
    const navigate = useNavigate();

    return (
        <Box>
            <AppBar position="sticky">
                <Box style={{padding: "15px 0", display: "flex", justifyContent: "center", gap: "30px"}}>
                    <Button variant="text" onClick={() => navigate("/")}>Products</Button>
                    <Button variant="text" onClick={() => navigate("/cart")}>
                        Cart
                        <Badge badgeContent={cart.cartItems.length} color="primary" style={{marginLeft: "10px"}}>
                            <ShoppingCartOutlined color="action" />
                        </Badge>
                    </Button>
                    <Button variant="text" onClick={() => navigate("/sign-in")}>Sign in</Button>
                </Box>
            </AppBar>
            <Box>
                {children}
            </Box>
        </Box>
    );
}

export default PublicLayout;