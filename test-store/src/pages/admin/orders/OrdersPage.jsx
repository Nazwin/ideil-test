import AdminLayout from "../../../layouts/AdminLayout";

function OrdersPage() {
    return (
        <AdminLayout>
            <div>Orders</div>
        </AdminLayout>
    );
}

export default OrdersPage;