import CreateProductForm from "./CreateProductForm/CreateProductForm";
import AdminLayout from "../../../layouts/AdminLayout";
import {
    Accordion,
    AccordionDetails,
    AccordionSummary, Avatar, Button,
    Container,
    FormControl, FormHelperText, IconButton, Input,
    InputLabel, MenuItem, Select,
    TextField,
    Typography
} from "@mui/material";
import ProductsTable from "./ProductsTable/ProductsTable";
import {ExpandMoreOutlined} from "@mui/icons-material";
import DeleteIcon from "@mui/icons-material/Delete";

function ProductsAdminPage() {
    return (
        <AdminLayout>
            <Container style={{padding: "25px 0"}}>
                <Accordion>
                    <AccordionSummary
                        expandIcon={<ExpandMoreOutlined />}
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                    >
                        <Typography>Create product</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <CreateProductForm />
                    </AccordionDetails>
                </Accordion>

                <ProductsTable />
            </Container>
        </AdminLayout>
    );
}

export default ProductsAdminPage;