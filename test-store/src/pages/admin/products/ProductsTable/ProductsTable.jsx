import React from 'react';
import {useNavigate} from 'react-router-dom';
import { DataGrid, GridToolbarContainer, GridToolbarExport } from '@mui/x-data-grid';
import { Box, Paper, Typography } from '@mui/material';
import {useGetProductsQuery} from "../../../../store/admin/products/productsApi";
import CircularProgress from "@mui/material/CircularProgress";

const columns = [
    { field: 'id', headerName: 'ID', width: 70 },
    { field: 'name', headerName: 'Name', width: 200 },
    { field: 'uid', headerName: 'UID', width: 120 },
    {
        field: 'images',
        headerName: 'Images',
        width: 120,
        renderCell: (params) => (
            <img
                src={params.value[0]}
                alt="Product Preview"
                style={{ width: '50px', height: '50px', objectFit: 'cover' }}
            />
        ),
    },
    { field: 'price', headerName: 'Price', type: 'number', width: 120 },
    { field: 'status', headerName: 'Status', width: 120, type: 'boolean' },
    { field: 'quantity', headerName: 'Available', width: 120 },
];

const ProductsTable = () => {
    const [paginationModel, setPaginationModel] = React.useState({
        pageSize: 20,
        page: 0,
    });

    const navigate = useNavigate();

    const {
        data: products = [],
        error: getProductsError,
        isLoading: isLoadingProducts,
        isFetching: isFetchingProducts
    } = useGetProductsQuery({page: paginationModel.page + 1});

    const handleRowClick = (params) => {
        navigate(`/admin/products/${params.row.id}`);
    };

    return (
        <Box style={{marginTop: "25px"}}>
            <Typography variant="h5" gutterBottom>
                Products Table
            </Typography>
            {isLoadingProducts ? (
                <CircularProgress />
            ) : (
                <Paper elevation={3} style={{ height: "auto", width: '100%' }}>
                    <DataGrid
                        rows={products.data}
                        columns={columns}
                        pageSize={20}
                        components={{
                            Toolbar: CustomToolbar,
                        }}
                        onRowClick={handleRowClick}
                        loading={isFetchingProducts}
                        rowCount={products.total}
                        pageSizeOptions={[20]}
                        paginationMode="server"
                        paginationModel={paginationModel}
                        onPaginationModelChange={setPaginationModel}
                    />
                </Paper>
            )}
        </Box>
    );
};

const CustomToolbar = () => {
    return (
        <GridToolbarContainer>
            <GridToolbarExport />
        </GridToolbarContainer>
    );
};

export default ProductsTable;
