import React, {useEffect, useState} from 'react';
import {
    TextField,
    Button,
    Select,
    MenuItem,
    FormControl,
    InputLabel,
    Input,
    FormHelperText,
    Avatar,
    IconButton, AccordionSummary, Accordion, Typography, AccordionDetails,
} from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import {ExpandMoreOutlined} from "@mui/icons-material";
import {useCreateProductMutation} from "../../../../store/admin/products/productsApi";

const defaultValue = {
    id: '',
    name: '',
    uid: '',
    images: [],
    price: 0,
    status: true,
    quantity: 0,
};

const CreateProductForm = ({product = {}}) => {
    const [formData, setFormData] = useState(defaultValue);

    const [imageError, setImageError] = useState('');
    const [formError, setFormError] = useState('');

    useEffect(() => {
        if(product.id) {
            setFormData(product);
        }
    }, [product]);

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setFormData({ ...formData, [name]: value });
    };

    const handleImageChange = (e) => {
        const files = Array.from(e.target.files);

        // Validate file types and size
        const validFiles = files.every(
            (file) =>
                ['image/jpeg', 'image/jpg', 'image/png'].includes(file.type) &&
                file.size <= 5 * 1024 * 1024
        );

        if (!validFiles) {
            setImageError(
                'Invalid file type or size. Please upload JPEG, JPG, or PNG files, each with a maximum size of 5 MB.'
            );
            return;
        }

        setImageError('');
        setFormData({ ...formData, images: [...formData.images, ...files] });
    };

    const handleImageRemove = (index) => {
        const newImages = [...formData.images];
        newImages.splice(index, 1);
        setFormData({ ...formData, images: newImages });
    };

    const [
        createProduct,
        {
            data: createdProduct,
            error: createProductErrors,
            isLoading: isCreatingProduct,
        },
    ] = useCreateProductMutation();

    useEffect(() => {
        setFormData(defaultValue);
    }, [createdProduct]);

    const handleSubmit = async (e) => {
        e.preventDefault();

        if (!formData.name || formData.images.length === 0) {
            setFormError('Product name and at least one image are required.');
            return;
        }

        setFormError('');

        // Assuming you have an API endpoint in Laravel
        const apiUrl = 'your_laravel_api_endpoint';
        try {
            const formDataToSend = new FormData();
            if(formData.id) {
                formDataToSend.append('id', formData.id);
            }
            formDataToSend.append('name', formData.name);
            formDataToSend.append('uid', formData.uid);
            formDataToSend.append('price', formData.price);
            formDataToSend.append('status', formData.status);
            formDataToSend.append('quantity', formData.quantity);

            formData.images.forEach((image, index) => {
                formDataToSend.append(`images[${index}]`, image);
            });

            createProduct(formDataToSend);

        } catch (error) {
            console.error('Error creating product:', error);
            // Handle error, show user a message, etc.
        }
    };

    return (
        <form onSubmit={handleSubmit}>
            <TextField
                label="Product Name"
                type="text"
                name="name"
                value={formData.name}
                onChange={handleInputChange}
                required
                fullWidth
                margin="normal"
            />

            <TextField
                label="UID"
                type="text"
                name="uid"
                value={formData.uid}
                onChange={handleInputChange}
                fullWidth
                margin="normal"
            />

            <FormControl fullWidth margin="normal">
                <InputLabel>Images</InputLabel>
                <Input
                    type="file"
                    name="images"
                    accept="image/jpeg, image/jpg, image/png"
                    onChange={handleImageChange}
                    required
                    multiple
                />
                <FormHelperText error>{imageError}</FormHelperText>
            </FormControl>

            {formData.images.length > 0 && (
                <div style={{ marginTop: '16px' }}>
                    <p>Image Previews:</p>
                    <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                        {formData.images.map((image, index) => (
                            <div key={index} style={{ marginRight: '8px', marginBottom: '8px' }}>
                                <Avatar
                                    src={!product.id ? URL.createObjectURL(image) : image}
                                    alt={`Preview ${index + 1}`}
                                    sx={{ width: 100, height: 100 }}
                                />
                                <IconButton onClick={() => handleImageRemove(index)}>
                                    <DeleteIcon />
                                </IconButton>
                            </div>
                        ))}
                    </div>
                </div>
            )}

            <TextField
                label="Price"
                type="number"
                name="price"
                value={formData.price}
                onChange={handleInputChange}
                fullWidth
                margin="normal"
            />

            <FormControl fullWidth margin="normal">
                <InputLabel>Status</InputLabel>
                <Select
                    name="status"
                    value={formData.status}
                    onChange={handleInputChange}
                >
                    <MenuItem value={1}>Visible on Store</MenuItem>
                    <MenuItem value={0}>Not Visible on Store</MenuItem>
                </Select>
            </FormControl>

            <TextField
                label="Quantity"
                type="number"
                name="quantity"
                value={formData.quantity}
                onChange={handleInputChange}
                fullWidth
                margin="normal"
            />

            <FormHelperText error>{formError}</FormHelperText>

            <Button type="submit" variant="contained" color="primary" disabled={isCreatingProduct}>
                {product?.id ? "Update" : "Create"} Product
            </Button>
        </form>
    );
};

export default CreateProductForm;
