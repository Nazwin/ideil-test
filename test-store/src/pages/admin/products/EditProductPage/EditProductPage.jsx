import {useParams} from "react-router-dom";
import AdminLayout from "../../../../layouts/AdminLayout";
import {Container} from "@mui/material";
import {useGetProductQuery} from "../../../../store/admin/products/productsApi";
import CircularProgress from "@mui/material/CircularProgress";
import CreateProductForm from "../CreateProductForm/CreateProductForm";

function EditProductPage() {

    const {id} = useParams();

    const {
        data: product = {},
        error: getProductError,
        isLoading: isLoadingProduct,
        isFetching: isFetchingProduct
    } = useGetProductQuery({id: id});

    return (
        <Container style={{padding: "25px 0"}}>
            {isLoadingProduct ? (<CircularProgress />) : (
                <CreateProductForm product={product} />
            )}
        </Container>
    );
}

export default EditProductPage;