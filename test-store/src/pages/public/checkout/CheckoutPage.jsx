import {useState, useEffect, useContext} from "react";
import {Backdrop, Button, Container, Grid, LinearProgress, TextField} from "@mui/material";
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import {useCreateOrderMutation, useGetDeliveryServicesQuery} from "../../../store/public/checkout/checkoutApi";
import PublicLayout from "../../../layouts/PublicLayout";
import {CartContext} from "../../../CartContext";
import CartItem from "../cart/CartItem/CartItem";
import {useNavigate} from "react-router-dom";
import CircularProgress from "@mui/material/CircularProgress";

function CheckoutPage() {
    const {cart, removeItem} = useContext(CartContext);
    const [deliveryService, setDeliveryService] = useState();
    const [address, setAddress] = useState("");
    const [fullName, setFullName] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");
    const [isValid, setIsValid] = useState(false);

    const navigate = useNavigate();

    if (!cart.cartItems || !cart.cartItems.length) navigate("/");

    const isValidForm = () => {
        return !(!deliveryService?.id || !address.trim() || !fullName.trim() || !phoneNumber.trim());
    };

    useEffect(() => {
        setIsValid(isValidForm());
    }, [deliveryService, address, fullName, phoneNumber]);

    const {
        data: deliveryServices = [],
        error: getDeliveryServicesError,
        isLoading: isLoadingDeliveryServices,
    } = useGetDeliveryServicesQuery();

    const [
        createOrder,
        {
            data: createdOrder,
            error: createOrderErrors,
            isLoading: isCreatingOrder,
        },
    ] = useCreateOrderMutation();

    useEffect(() => {
    //     Show errors for user
    }, [createOrderErrors]);

    useEffect(() => {
        if(createdOrder) {
            cart.cartItems.forEach(item => {
                removeItem(item.id);
            });
        }
    }, [createdOrder]);

    const handleChangeDeliveryService = (event) => {
        setDeliveryService(event.target.value);
    };

    const handleOrder = () => {
        if(!isValidForm()) return;

        createOrder({
            cartItems: cart.cartItems,
            client: {
                fullName,
                phoneNumber,
                address,
                deliveryService
            }
        });
    }

    return (
        <PublicLayout>
            {isLoadingDeliveryServices ? (
                <LinearProgress />
            ) : (
                <Container maxWidth="lg">
                    <Grid container spacing={2} sx={{padding: "15px 0"}}>
                        <Grid item xs={12}>
                            <Grid container spacing={2}>
                                {cart.cartItems.map(item => (
                                    <Grid item xs={12} md={4}>
                                        <CartItem item={item} hideActions={true} />
                                    </Grid>
                                ))}
                            </Grid>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                label="Full name *"
                                variant="standard"
                                value={fullName}
                                onChange={(event) => setFullName(event.target.value)}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                label="Phone number *"
                                variant="standard"
                                value={phoneNumber}
                                onChange={(event) => setPhoneNumber(event.target.value)}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <FormControl fullWidth>
                                <InputLabel id="delivery-service-label">Delivery service *</InputLabel>
                                <Select
                                    labelId="delivery-service-label"
                                    value={deliveryService}
                                    label="Delivery service *"
                                    onChange={handleChangeDeliveryService}
                                >
                                    {deliveryServices.map(service => (
                                        <MenuItem value={service} key={service.id}>{service.name}</MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                label="Address *"
                                variant="standard"
                                value={address}
                                onChange={(event) => setAddress(event.target.value)}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Button disabled={!isValid || isCreatingOrder} onClick={() => handleOrder()}>Order</Button>
                        </Grid>
                    </Grid>
                    <Backdrop
                        sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                        open={isCreatingOrder}
                    >
                        <CircularProgress color="inherit" />
                    </Backdrop>
                </Container>
            )}
        </PublicLayout>
    );
}

export default CheckoutPage;