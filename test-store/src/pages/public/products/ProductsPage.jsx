import PublicLayout from "../../../layouts/PublicLayout";
import {useGetProductsQuery} from "../../../store/public/products/productsApi";
import {Backdrop, Container, Grid, LinearProgress, Pagination} from "@mui/material";
import ProductCard from "./ProductCard/ProductCard";
import CircularProgress from '@mui/material/CircularProgress';
import Box from "@mui/material/Box";
import {useState} from "react";

function ProductsPage() {
    const [currentPage, setCurrentPage] = useState(1);

    const {
        data: products = [],
        error: getProductsError,
        isLoading: isLoadingProducts,
        isFetching: isFetchingProducts
    } = useGetProductsQuery({page: currentPage});

    return (
        <PublicLayout>
            {isLoadingProducts ? (
                <LinearProgress />
            ) : (
                <Container maxWidth="lg">
                    <Box style={{display: "flex", flexDirection: "column", alignItems: "center", padding: "15px 0"}}>
                        <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
                            {isLoadingProducts ?
                                <CircularProgress /> :
                                products.data.map(product => (
                                    <Grid item xs={4} key={product.id}>
                                        <ProductCard product={product} />
                                    </Grid>
                                ))}
                        </Grid>
                        <Pagination
                            style={{marginTop: "15px"}}
                            count={products.last_page}
                            shape="rounded"
                            onChange={(event, page) => setCurrentPage(page)}
                        />
                        <Backdrop
                            sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                            open={isFetchingProducts}
                        >
                            <CircularProgress color="inherit" />
                        </Backdrop>
                    </Box>
                </Container>
            )}
        </PublicLayout>
    );
}

export default ProductsPage;