import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { Button, CardActionArea, CardActions } from '@mui/material';
import {useContext} from "react";
import {CartContext} from "../../../../CartContext";
import Box from "@mui/material/Box";

function ProductCard({
    product
}){
    const { addToCart } = useContext(CartContext);

    return (
        <Card sx={{ maxWidth: 345 }}>
            <CardActionArea>
                <CardMedia
                    component="img"
                    height="140"
                    image={product.images[0]}
                    alt="green iguana"
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        {product.name}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        {product.price} UAH
                    </Typography>
                </CardContent>
            </CardActionArea>
            <CardActions>
                <Box style={{width: "100%", display: "flex", justifyContent: "space-between"}}>
                    <Button disabled={product.available <= 0} size="small" color="primary" onClick={() => addToCart(product)}>
                        Add to cart
                    </Button>
                    {product.available <= 0 ? (
                        <Typography>Out of stock</Typography>
                    ) : (
                        <Typography>Available: {product.available}</Typography>
                    )}
                </Box>
            </CardActions>
        </Card>
    );
}

export default ProductCard;