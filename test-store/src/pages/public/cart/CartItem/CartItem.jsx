import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import DeleteIcon from '@mui/icons-material/Delete';
import {AddOutlined, RemoveOutlined} from "@mui/icons-material";
import {useContext} from "react";
import {CartContext} from "../../../../CartContext";


export default function CartItem({item, hideActions = false}) {
    const {increase, decrease, removeItem} = useContext(CartContext);

    return (
        <Card sx={{ display: 'flex' }} style={{height: "100%"}}>
            <CardMedia
                component="img"
                sx={{ width: 151 }}
                image={item.images[0]}
                alt="Live from space album cover"
            />
            <Box sx={{ display: 'flex', flexDirection: 'column', flexGrow: 1 }}>
                <CardContent sx={{ flex: '1 0 auto' }}>
                    <Typography component="div" variant="h5">
                        {item.name}
                    </Typography>
                    <Typography variant="subtitle1" color="text.secondary" component="div">
                        {item.price} UAH
                    </Typography>
                </CardContent>
                <Box style={{display: "flex", justifyContent: "space-between"}}>
                    <Box sx={{ display: 'flex', alignItems: 'center', pl: 1, pb: 1, gap: "6px" }}>
                        {!hideActions && (
                            <IconButton aria-label="previous" onClick={() => decrease(item)}>
                                <RemoveOutlined />
                            </IconButton>
                        )}
                        <Typography component="div" variant="h5">
                            {item.count}
                        </Typography>
                        {!hideActions && (
                            <IconButton aria-label="next" onClick={() => increase(item)}>
                                <AddOutlined />
                            </IconButton>
                        )}
                    </Box>
                    <Box sx={{pb: 1, pr: 1}}>
                        {!hideActions && (
                            <IconButton aria-label="delete" onClick={() => removeItem(item.id)}>
                                <DeleteIcon />
                            </IconButton>
                        )}
                    </Box>
                </Box>
            </Box>
        </Card>
    );
}