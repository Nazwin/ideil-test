import PublicLayout from "../../../layouts/PublicLayout";
import CartItem from "./CartItem/CartItem";
import {useContext, useEffect, useState} from "react";
import {CartContext} from "../../../CartContext";
import {Button, Container, Grid, TextField} from "@mui/material";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import {useNavigate} from "react-router-dom";

function CartPage() {
    const { cart } = useContext(CartContext);
    const [total, setTotal] = useState(0);

    const navigate = useNavigate();

    useEffect(() => {
        let sum = cart.cartItems.reduce((accumulator, currentValue) => {
            return accumulator + currentValue.price * currentValue.count;
        }, 0);

        setTotal(sum.toFixed(2));
    }, [cart])

    return (
        <PublicLayout>
            <Container>
                {!cart?.cartItems || !cart.cartItems.length ? (
                    <Typography variant={"h3"} sx={{mt: "15px"}}>Cart is empty</Typography>
                ) : (
                    <Box style={{padding: "15px 0"}}>
                        <Grid container spacing={2}>
                            {cart.cartItems.map(item => <Grid item key={item.id} xs={12} md={4}><CartItem item={item} /></Grid>)}
                        </Grid>
                        <Typography sx={{mt: "10px", mb: "10px"}}>Total: {total} UAH</Typography>
                        <Button onClick={() => navigate("/checkout")}>Checkout</Button>
                    </Box>
                )}
            </Container>
        </PublicLayout>
    );
}

export default CartPage;