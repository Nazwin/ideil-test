import React from 'react';
import { useForm, Controller } from 'react-hook-form';
import { TextField, Button, Container, Paper, Typography, Grid } from '@mui/material';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import PublicLayout from "../../../layouts/PublicLayout";
import {signInWithEmailAndPassword} from "../../../services/authOperations";

const schema = yup.object().shape({
    email: yup.string().email('Invalid email').required('Email is required'),
    password: yup.string().required('Password is required'),
});

const SignInPage = () => {
    const { control, handleSubmit, formState: { errors }, setError } = useForm({
        resolver: yupResolver(schema),
        defaultValues: {
            email: 'admin@app.com',
            password: 'password',
        }
    });

    const onSubmit = async ({email, password}) => {
        signInWithEmailAndPassword(email, password)
            .then((user) => {
                // No need to do anything, user data will be set at app/auth/AuthContext
            })
            .catch((error) => {
                setError(error.type, {
                    type: "manual",
                    message: error.message,
                });
            });
    };

    return (
        <PublicLayout>
            <Container component="main" maxWidth="xs" style={{marginTop: "75px"}}>
                <Paper elevation={3} sx={{ padding: 3, display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                    <Typography component="h1" variant="h5" sx={{ marginBottom: 3 }}>
                        Sign In
                    </Typography>
                    <form onSubmit={handleSubmit(onSubmit)} style={{ width: '100%' }}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <Controller
                                    name="email"
                                    control={control}
                                    defaultValue=""
                                    render={({ field }) => (
                                        <TextField
                                            {...field}
                                            label="Email Address"
                                            fullWidth
                                            error={!!errors.email}
                                            helperText={errors.email?.message}
                                        />
                                    )}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <Controller
                                    name="password"
                                    control={control}
                                    defaultValue=""
                                    render={({ field }) => (
                                        <TextField
                                            {...field}
                                            label="Password"
                                            type="password"
                                            fullWidth
                                            error={!!errors.password}
                                            helperText={errors.password?.message}
                                        />
                                    )}
                                />
                            </Grid>
                        </Grid>
                        <Button type="submit" variant="contained" color="primary" fullWidth sx={{ marginTop: 2 }}>
                            Sign In
                        </Button>
                    </form>
                </Paper>
            </Container>
        </PublicLayout>
    );
};

export default SignInPage;
