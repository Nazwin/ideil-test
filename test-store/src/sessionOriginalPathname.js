const sessionStorageKey = "fuseOriginalPathname";

export const getSessionOriginalPathname = () => {
  return window.sessionStorage.getItem(sessionStorageKey);
};

export const setSessionOriginalPathname = (url) => {
  window.sessionStorage.setItem(sessionStorageKey, url);
};

export const resetSessionOriginalPathname = () => {
  window.sessionStorage.removeItem(sessionStorageKey);
};
