import apiService from "./apiService";
import { apiBaseQuery } from "./apiService";

export { apiService, apiBaseQuery };
