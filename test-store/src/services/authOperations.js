import {apiService} from "./index";

const onLogout = () => {
  apiService.setAccessToken(null);
  apiService.emit("onLogout");
};

const logout = () => {
  new Promise(() => {
    apiService.axios.post("auth/logout").finally(() => {
      onLogout();
    });
  });
};

const signInWithEmailAndPassword = (email, password) => {
  return new Promise((resolve, reject) => {
    apiService.axios
      .post("auth/sign-in", {
        email,
        password,
      })
      .then((response) => {
        console.log(response.data.user)
        if (response.data.user) {
          console.log('setAccessToken')
          apiService.setAccessToken(response.data.access_token);

          console.log('resolve')
          resolve(response.data.user);

          console.log('emit')
          apiService.emit("onLogin", response.data.user);
          console.log('emit after')
        } else {
          reject(response.data.error);
        }
      })
      .catch((reason) => {
        console.log(reason)
        // reject(response.data);
      });
  });
};

const signInWithToken = () => {
  return new Promise((resolve, reject) => {
    apiService.axios
      .get("auth/me")
      .then((response) => {
        if (response.data.user) {
          resolve(response.data.user);
        } else {
          onLogout();
          reject(new Error("Failed to login with token."));
        }
      })
      .catch(() => {
        onLogout();
        reject(new Error("Failed to login with token."));
      });
  });
};

const updateUserData = (user) => {
  const userData = {
    name: user.displayName,
    photo_url: user.photoURL,
    email: user.email,
  };

  return apiService.axios.post("auth/me", userData);
};

const sendGarantRegistrationApplication = (email) => {
  return apiService.axios.post("auth/grant-request", {
    email,
  });
};

const forgotPassword = (email) => {
  return apiService.axios.post("auth/forgot-password", {
    email,
  });
};

const checkForgotPasswordToken = (token) => {
  return apiService.axios.get(`auth/check-forgot-password-token/${token}`);
};

const updatePassword = (formData, token) => {
  return apiService.axios.post(`auth/update-password/${token}`, {
    ...formData
  });
};

const enable2fa = async () => {
  await apiService.axios.post("auth/2fa_enable");

  apiService.emit("on2faEnabled");
};

const get2faSwgData = () => {
  return apiService.axios.get("auth/2fa_qr_code");
};

const get2faSecretKey = () => {
  return apiService.axios.get("auth/2fa_secret_key");
};

const get2faRecoveryCodes = () => {
  return apiService.axios.get("auth/2fa_recovery_codes");
};

const confirmTwoFactorCode = (code) => {
  return new Promise((resolve) => {
    apiService.axios
      .post("auth/2fa_confirm", {
        code,
      })
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        resolve(error.response);
      });
  });
};

const changePassword = (credentials) => {
  return new Promise((resolve) => {
    apiService.axios
      .post("auth/change_password", credentials)
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        resolve(error.response);
      });
  });
};

export {
  logout,
  onLogout,
  signInWithEmailAndPassword,
  signInWithToken,
  updateUserData,
  sendGarantRegistrationApplication,
  forgotPassword,
  checkForgotPasswordToken,
  updatePassword,
  enable2fa,
  get2faSwgData,
  get2faSecretKey,
  confirmTwoFactorCode,
  get2faRecoveryCodes,
  changePassword,
};
