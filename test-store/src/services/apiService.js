import axios from "axios";
import FuseUtils from "../utils";

class ApiService extends FuseUtils.EventEmitter {
    constructor() {
        super();

        this.axios = axios.create({
            baseURL: `${process.env.REACT_APP_API_BASE_URL}/api/`,
            headers: { Accept: "application/json" },
        });

        this.unauthorizedInterceptorId = null;
    }

    init() {
        this.handleAutoAuthentication();
    }

    handleAutoAuthentication = () => {
        const access_token = this.getAccessToken();

        if (!access_token) {
            this.emit("onNoAccessToken");

            return;
        }

        if (this.isAuthTokenValid(access_token)) {
            this.setAccessToken(access_token);
            this.emit("onAutoLogin", true);
        } else {
            this.setAccessToken(null);
            this.emit("onAutoLogout", "Access token expired");
        }
    };

    setUnauthorizedInterceptor = () => {
        this.unauthorizedInterceptorId = this.axios.interceptors.response.use(
            (response) => {
                return response;
            },
            (err) => {
                return new Promise(() => {
                    if (
                        err.response.status === 401 &&
                        err.config &&
                        !err.config.__isRetryRequest
                    ) {
                        // if you ever get an unauthorized response, logout the user
                        this.emit("onAutoLogout", "Invalid access token");
                        this.setAccessToken(null);
                    }
                    throw err;
                });
            }
        );
    };

    removeUnauthorizedInterceptor = () => {
        if (this.unauthorizedInterceptorId === null) {
            return;
        }

        this.axios.interceptors.response.eject(this.unauthorizedInterceptorId);
    };

    setAccessToken = (access_token) => {
        if (access_token) {
            localStorage.setItem("access_token", access_token);

            this.axios.defaults.headers.common.Authorization = `Bearer ${access_token}`;

            this.setUnauthorizedInterceptor();
        } else {
            localStorage.removeItem("access_token");

            delete this.axios.defaults.headers.common.Authorization;

            this.removeUnauthorizedInterceptor();
        }
    };

    getAccessToken = () => {
        return window.localStorage.getItem("access_token");
    };

    isAuthTokenValid = (access_token) => {
        if (!access_token) {
            return false;
        }

        return true;
    };
}

const apiService = new ApiService();

export const apiBaseQuery =
    ({ baseRoute } = { baseRoute: "" }) =>
        async ({ url, method, data, params, headers }) => {
            if (baseRoute && !baseRoute.endsWith("/")) {
                baseRoute += "/";
            }

            if (!url) {
                url = "";
            }

            if (url.startsWith("/")) {
                url = url.slice(1);
            }

            try {
                const result = await apiService.axios({
                    url: baseRoute + url,
                    method,
                    data,
                    params,
                    headers,
                });
                return { data: result.data };
            } catch (axiosError) {
                const err = axiosError;
                return {
                    error: {
                        status: err.response?.status,
                        data: err.response?.data || err.message,
                    },
                };
            }
        };

export default apiService;
