import ProductsPage from "../pages/public/products/ProductsPage";
import ProductsAdminPage from "../pages/admin/products/ProductsAdminPage";
import CartPage from "../pages/public/cart/CartPage";
import OrdersPage from "../pages/admin/orders/OrdersPage";
import SignInPage from "../pages/auth/sign-in/SignInPage";
import {
    createBrowserRouter,
    RouterProvider,
} from "react-router-dom";
import CheckoutPage from "../pages/public/checkout/CheckoutPage";
import AdminLayout from "../layouts/AdminLayout";
import EditProductPage from "../pages/admin/products/EditProductPage/EditProductPage";

const routes = createBrowserRouter([
    {
        path: "/",
        element: <ProductsPage />
    },
    {
        path: "/cart",
        element: <CartPage />
    },
    {
        path: "/admin/products",
        element: <ProductsAdminPage />
    },
    {
        path: "/admin/products/:id",
        element: <AdminLayout><EditProductPage /></AdminLayout>
    },
    {
        path: "/admin/orders",
        element: <OrdersPage />
    },
    {
        path: "sign-in",
        element: <SignInPage />
    },
    {
        path: "checkout",
        element: <CheckoutPage />
    }
]);

export default routes;